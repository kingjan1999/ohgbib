/**
 * Amy is Awesome!
 */
'use strict';
const app = require('app');
const ipc = require('ipc');
const fs = require('fs');
const BrowserWindow = require('browser-window');
const Menu = require('menu');
const shell = require('shell');
const dialog = require('dialog');

const angular = require('./client/lib/ng-electron/ng-bridge');


var handleStartupEvent = function() {
    if (process.platform !== 'win32') {
        return false;
    }

    var squirrelCommand = process.argv[1];
    switch (squirrelCommand) {
        case '--squirrel-install':
        case '--squirrel-updated':

            // Optionally do things such as:
            //
            // - Install desktop and start menu shortcuts
            // - Add your .exe to the PATH
            // - Write to the registry for things like file associations and
            //   explorer context menus

            // Always quit when done
            app.quit();

            return true;
        case '--squirrel-uninstall':
            // Undo anything you did in the --squirrel-install and
            // --squirrel-updated handlers

            // Always quit when done
            app.quit();

            return true;
        case '--squirrel-obsolete':
            // This is called on the outgoing version of your app before
            // we update to the new version - it's the opposite of
            // --squirrel-updated
            app.quit();
            return true;
    }
};

if (handleStartupEvent()) {
    return;
}

function createMainWindow() {
    const win = new BrowserWindow({
        width: 800,
        height: 600,
        resizable: true,
        icon: 'assets/book.png'
    });

    win.loadUrl(`file://${__dirname}/index.html`);
    win.on('closed', onClosed);

    return win;
}

function onClosed() {
    mainWindow = null;
}
// prevent window being GC'd
let mainWindow;

app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate-with-no-open-windows', function () {
    if (!mainWindow) {
        mainWindow = createMainWindow();
    }
});

app.on('will-quit', function () {
    console.log('<====================================>');
    console.log('Amy Says, "Stay Awesome Kids!"');
    console.log('<====================================>');
});

app.on('ready', function () {
    mainWindow = createMainWindow();
    console.log('<====================================>');
    console.log("Amy says, \"Let's Code Awesome!\"");
    console.log('<====================================>');
    mainWindow.webContents.on('dom-ready', function (e) {
        //try and manually bootstrap AngularJS
        var code = "angular.bootstrap(document, ['app']);"
        mainWindow.webContents.executeJavaScript(code);
    });

    mainWindow.webContents.on('did-finish-load', function (e) {
        var menu = new Menu();
        var tpl = [
            {
                label: 'OHGBIB',
                submenu: [
                    {
                        label: 'Über',
                        click: function () {
                            console.log('Version 1.Amy.Awesome!');
                            dialog.showMessageBox(mainWindow, {
                                type: "info",
                                buttons: ["OK"],
                                title: "Über OHGBIB",
                                message: "OHGBIB - Ein Bibliotheksprogramm",
                                detail: "OHGBIB wurde speziell für das OHG Springe entwickelt."
                            })
                        }
                    },
                    {
                        label: 'Beenden',
                        click: function () {
                            app.quit();
                        },
                        accelerator: 'Command+Q'
                    }
                ]
            },
            {
                label: 'Actions',
                submenu: [
                    {
                        label: 'Host to Client',
                        click: function (msg) {
                            var msg = 'Host: Hiya Amy App Stack!';
                            angular.send(msg);
                        }
                    }
                ]
            }, {
                label: 'Entwicklung',
                submenu: [{
                    label: 'Neuladen',
                    accelerator: 'CmdOrCtrl+R',
                    click: function () {
                        BrowserWindow.getFocusedWindow().reloadIgnoringCache();
                    }
                }, {
                    label: 'DevTools an/aus',
                    accelerator: 'Alt+CmdOrCtrl+I',
                    click: function () {
                        BrowserWindow.getFocusedWindow().toggleDevTools();
                    }
                }, {
                    label: 'Beenden',
                    accelerator: 'CmdOrCtrl+Q',
                    click: function () {
                        app.quit();
                    }
                }]
            }

        ];
        menu = Menu.buildFromTemplate(tpl);
        Menu.setApplicationMenu(menu);

        //Start listening for client messages
        angular.listen(function (msg) {
            console.log('Client: ' + msg);
            if (Array.isArray(msg)) {
                if (msg[0] === 'print-pdf') {
                    console.log('printe...');
                    mainWindow.webContents.printToPDF({landscape: true}, function (error, data) {
                        if (error) throw error;
                        fs.writeFile(msg[1], data, function (error) {
                            if (error)
                                throw error;
                            console.log('PDF geschrieben');
                            shell.openItem(msg[1]); //Filename
                        })
                    });
                }
            }
        });

    });

    mainWindow.openDevTools();
});
