/**
 * Created by jan on 14.12.15.
 */
const fs = require('fs');


angular.module('app.services')
    .factory('SettingsService', function ($q, electron) {

        var persistSettings = function (settingsObject) {
            return $q(function (res, rej) {
                fs.writeFile(path.join(electron.app.getDataPath(), 'settings.json'), JSON.stringify(settingsObject), function (err) {
                    if (err)
                        rej(err);
                    else
                        res(true);
                });
            });
        };

        var readSettings = function () {
            return $q(function (res, rej) {
                fs.readFile(path.join(electron.app.getDataPath(), 'settings.json'), function (err, data) {
                    if (err)
                        rej(err);
                    else
                        res(JSON.parse(data));
                });
            })
        };


        var getKeys = function (keys) {
            var promise = $q.defer();
            readSettings()
                .then(function (settings) {
                    var re = {};
                    if(Array.isArray(keys)) {
                        keys.forEach(function (value) {
                            re[value] = settings[value];
                        });
                    } else {
                        re = settings[keys];
                    }
                    promise.resolve(re);
                }).catch(function (err) {
                promise.reject(err);
            });
            return promise.promise;
        };

        return {
            read: readSettings,
            write: persistSettings,
            key: getKeys,
            keys: getKeys
        }
    });

