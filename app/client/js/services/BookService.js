/**
 * Created by jan on 23.11.15.
 */
angular.module('app.services')
    .factory('BookService', function ($q) {

        var getBook = function (id) {
            return $q(function (res, rej) {
                db.books.findOne({_id: id}, function (err, doc) {
                    if (err) {
                        rej(err);
                    } else {
                        res(doc);
                    }
                })
            })
        };

        return {
            getBook: getBook
        };
    });

