/**
 * Created by jan on 11.12.15.
 */
angular.module('app.services')
    .factory('UserService', function ($q, BookService) {

        var byId = function (id) {
            return $q(function (res, rej) {
                db.users.findOne({_id: id}, function (err, user) {
                    if (err) {
                        rej(err)
                    } else {
                        res(user);
                    }
                })
            });
        };

        var getLendings = function (id) {
            return $q(function (res, rej) {
                db.lends.find({user: id}, function (err, lendings) {
                    if (err) {
                        rej(err)
                    } else {
                        res(lendings);
                    }
                })
            });
        };

        var getBookLendings = function (id) {
            return getLendings(id).then(function (lendings) {
                var promises = [];
                lendings.forEach(function (lending) {
                    promises.push(BookService.getBook(lending.book).then(function (book) {
                        lending.book = book;
                        return lending;
                    }))
                });
                return $q.all(promises);
            })
        };

        return {
            byId: byId,
            getLendings: getLendings,
            getBookLendings: getBookLendings
        };

    });
