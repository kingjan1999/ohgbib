/**
 * Created by jan on 23.11.15.
 */
angular.module('app.services')
    .factory('LendService', function ($q, $http, SettingsService, UserService) {

        var isLend = function (id) {
            return $q(function (re, rej) {
                db.lends.findOne({book: id, active: true}, function (err, doc) {
                    if (err) {
                        rej("DB Error");
                        return;
                    }
                    console.log('Ergebnis: ', doc);
                    if (doc) {
                        re(doc);
                    } else {
                        re(false);
                    }
                })
            })
        };

        var whoLend = function (id) {
            return isLend(id).then(function (doc) {
                if(!doc) {
                   return {};
                } else {
                    return UserService.byId(doc.user);
                }
            })
        };

        var getHistory = function (id) {
            return $q(function (re, rej) {
                db.lends.find({book: id}).sort({active: -1}).exec(function (err, doc) {
                    if (err) {
                        console.log('DB,', err);
                        rej("DB Error");
                        return;
                    }
                    re(doc);
                })
            })
        };

        var addLend = function (book, user, until) {
            return $q(function (re, rej) {
                db.lends.insert({
                    book: book,
                    user: user,
                    until: until,
                    from: moment().valueOf(),
                    active: true
                }, function (err, doc) {
                    if (err) {
                        rej(err);
                    } else {
                        re(doc);
                    }
                })
            })
        };

        var giveBack = function (book) {
            return $q(function (re, rej) {
                db.lends.update({active: true, book: book}, {
                    $set: {
                        active: false,
                        until: moment().valueOf()
                    }
                }, {}, function (err, numReplaced) {
                    if (err) {
                        rej(err);
                    } else {
                        re(numReplaced);
                    }
                })
            });
        };

        var getFerien = function (jahr) {
            var def = $q.defer();
            $http({
                method: 'GET',
                url: 'http://api.smartnoob.de/ferien/v1/ferien/',
                params: {bundesland: 'ni', jahr: jahr}
            })
                .then(function (response) {
                    console.log('Jahr', jahr);
                    var dat = response.data;
                    if (!dat || !dat.daten || dat.error !== 0) {
                        def.reject(response);
                    } else {
                        var daten = dat.daten;
                        daten.map(function (ferien) {
                            ferien.mbegin = moment.unix(ferien.beginn);
                            ferien.mende = moment.unix(ferien.ende);
                            return ferien;
                        });
                        daten.forEach(function (val, key) {
                            if (moment().isAfter(val.mende)) { //Wenn Ferien vergangen
                                delete daten[key]; //Entferne sie
                            }
                        });
                        def.resolve(daten);
                    }
                }, function (err) {
                    def.reject(err);
                });

            return def.promise;
        };

        var calculateUntil = function () {
            console.log('Called');
            return SettingsService.keys(['ferien', 'lendDays', 'workDays']).then(function (settings) {
                var ferien = settings.ferien;
                const to_add = settings.lendDays;
                const workDays = settings.workDays;
                var added = 0;
                var current = moment();
                while (added < to_add) {
                    current = current.add(1, 'd');
                    var day = current.format('dd');
                    holiday.setState('ni');
                    if (workDays && (day === 'Sa' || day === 'So' || holiday.isHoliday(current.toDate()))) {//Ignore weekends and holidays TODO: Settings
                        continue;
                    }
                    var inFerien = false;
                    ferien.forEach(function (ferie) {
                        if (current.isBetween(ferie.beginn, ferie.ende)) {
                            inFerien = true;
                        }
                    });
                    if (!inFerien) {
                        added++;
                    }
                }
                return current;
            }, function (err) {
                console.log(err);
            });
        };

        return {
            isLend: isLend,
            getHistory: getHistory,
            addLend: addLend,
            calculateUntil: calculateUntil,
            giveBack: giveBack,
            getFerien: getFerien,
            whoLend: whoLend
        };
    })
;

