var isbnp = require('isbn-utils');


angular.module('app.directives', [])

    .directive('isbn', function () {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {
                ctrl.$validators.isbn = function (modelValue, viewValue) {
                    if (ctrl.$isEmpty(modelValue)) {
                        // consider empty models to be valid
                        return true;
                    }
                    return isbnp.isValid(viewValue.replace(/-*/ig, ''));
                };
            }
        };
    });
