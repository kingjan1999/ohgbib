angular.module('app.filters', [])
    .filter('authors', function () {
        return function (input) {
            return input.toString();
        }
    }).filter('verbleibend', function () {
    return function (input) {
        return moment(input).format('L');
    }
});
