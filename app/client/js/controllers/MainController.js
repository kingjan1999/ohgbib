/**
 * Created by jan on 13.09.15.
 */
angular.module('app.controllers')
    .controller('MainController',
        function ($scope, $rootScope, electron) {

            //listen for host messages
            $rootScope.$on('electron-host', function (evt, data) {
                console.log(data);
            });

            $scope.searchFilter = {search: ''};

            //Click face handler
            $scope.search = function () {
                if (!$scope.searchFilter.search || $scope.searchFilter.search === '') {
                    return;
                }
                console.log($scope.searchFilter.search);
            };

            $scope.searchKey = function (event) {
                if (event.keyCode === 13) {
                    console.log('Enter');
                    $scope.search();
                }
            };

            $scope.bookFields = [
                {
                    key: 'isbn',
                    type: 'lx-input',
                    templateOptions: {
                        label: 'ISBN',
                        required: true,
                        type: 'text',
                        onBlur: function ($viewValue, $modelValue, scope) {
                            console.log($viewValue);
                            $scope.searchISBN($viewValue);
                        }
                    }

                }, {
                    key: 'title',
                    type: 'lx-input',
                    templateOptions: {
                        label: 'Titel',
                        required: true,
                        type: 'text'
                    }

                }, {
                    key: 'subtitle',
                    type: 'lx-input',
                    templateOptions: {
                        label: 'Untertitel',
                        required: false,
                        type: 'text'
                    }

                }, {
                    key: 'author',
                    type: 'lx-input',
                    templateOptions: {
                        label: 'Autor',
                        required: true,
                        type: 'text'
                    }
                }, {
                    key: 'publisher',
                    type: 'lx-input',
                    templateOptions: {
                        label: 'Verlag',
                        required: true,
                        type: 'text'
                    }
                }, {
                    key: 'author',
                    type: 'lx-input',
                    templateOptions: {
                        label: 'Autor',
                        required: true,
                        type: 'text'
                    }
                },
                {
                    key: 'typ',
                    type: 'lx-radio',
                    templateOptions: {
                        label: 'Typ des Mediums',
                        required: true,
                        options: [
                            {name: 'Buch', value: 'book'},
                            {name: 'DVD', value: 'dvd'},
                            {name: 'Hörspiel', value: 'audio'},
                            {name: 'Magazin', value: 'magazine'},
                            {name: 'Anderes', value: 'other'}
                        ]
                    }
                },
                {
                    key: 'hrsg',
                    type: 'lx-input',
                    templateOptions: {
                        label: 'Herausgeber',
                        type: 'text'
                    }
                }, {
                    key: 'ort',
                    type: 'lx-input',
                    templateOptions: {
                        label: 'Erscheinungsort',
                        type: 'text'
                    }
                }, {
                    key: 'edition',
                    type: 'lx-input',
                    templateOptions: {
                        label: 'Auflage',
                        type: 'text'
                    }
                }, {
                    key: 'year',
                    type: 'lx-input',
                    templateOptions: {
                        label: 'Erscheinungsjahr',
                        type: 'number'
                    }
                }, {
                    key: 'price',
                    type: 'lx-input',
                    templateOptions: {
                        label: 'Preis',
                        type: 'number'
                    }
                }, {
                    key: 'pages',
                    type: 'lx-input',
                    templateOptions: {
                        label: 'Seitenanzahl',
                        type: 'number'
                    }
                }, {
                    key: 'description',
                    type: 'lx-textarea',
                    templateOptions: {
                        label: 'Beschreibung'
                    }
                }, {
                    'key': 'cats',
                    'type': 'lx-select', // 'lx-select-multiple'
                    'templateOptions': {
                        //'multiple': true, // {{ default: false}}
                        'label': '', // default: 'Select'
                        'placeholder': 'Kategorien',
                        'required': false, // ng-required
                        'disabled': false, // ng-disabled
                        'selected': 'A', // displays current selected property as placeholder
                        'selected2': 'B', // optional second placeholder display
                        'choice': 'A', // dropdown choice display
                        'choice2': 'C', // optional second choice display
                        'choices': [{'a': 1}, {'b': 2}],
                    }
                }, {
                    key: 'signature',
                    type: 'lx-input',
                    templateOptions: {
                        label: 'Signatur',
                        type: 'text',
                        required: true
                    }
                }, {
                    key: 'barcode',
                    type: 'lx-input',
                    templateOptions: {
                        label: 'Barcode',
                        type: 'text',
                        required: true
                    }
                }
            ];

            $scope.userFields = [
                {
                    key: 'vorname',
                    type: 'lx-input',
                    templateOptions: {
                        type: 'text',
                        label: 'Vorname',
                        required: true
                    }
                }, {
                    key: 'nachname',
                    type: 'lx-input',
                    templateOptions: {
                        type: 'text',
                        label: 'Nachname',
                        required: true
                    }
                },
                {
                    key: 'typ',
                    type: 'lx-radio',
                    templateOptions: {
                        label: 'Benutzerklasse: ',
                        options: [
                            {name: 'Schüler', value: 'student'},
                            {name: 'Lehrer', value: 'teacher'},
                            {name: 'Anderes', value: 'other'}
                        ],
                        required: true
                    }
                },
                {
                    key: 'course',
                    type: 'lx-input',
                    'templateOptions': {
                        type: 'text',
                        label: 'Klasse'
                    }
                },
                {
                    key: 'barcode',
                    type: 'lx-input',
                    templateOptions: {
                        type: 'text',
                        label: 'Barcode',
                        required: true
                    }
                },
                {
                    key: 'other',
                    type: 'lx-textarea',
                    templateOptions: {
                        label: 'Sonstige Bemerkungen'
                    }
                }

            ];

        });
