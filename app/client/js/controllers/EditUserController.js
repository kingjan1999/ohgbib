/**
 * Created by jan on 13.09.15.
 */
angular.module('app.controllers')

    .controller('EditUserController', function ($scope, electron, $timeout, LxNotificationService, $stateParams, UserService) {
        //var api_key = "AIzaSyACpwb_vQe8cGu_v3GcQ5nbicrVZ6wKp7A"; //TODO

        $scope.action = 'editieren';
        console.log($stateParams);
        $scope.user_id = $stateParams.user_id;

        $scope.reset = function () {
            UserService.byId($scope.user_id).then(function (doc) {
                console.log(doc);
                $scope.user = doc;
            });
        };

        $scope.reset();

        $scope.submit = function (e) {
            console.log('Speichere');
            db.users.update({_id: $scope.user_id}, $scope.user);
            LxNotificationService.notify('Benutzer wurde gespeichert!');
            $scope.reset(); //Todo redirect
            e.preventDefault();
        };
    });
