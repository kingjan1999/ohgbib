/**
 * Created by jan on 13.09.15.
 */
angular.module('app.controllers')

    .controller('EditBookController',function ($scope, electron, $timeout, LxNotificationService, $stateParams) {
        //var api_key = "AIzaSyACpwb_vQe8cGu_v3GcQ5nbicrVZ6wKp7A"; //TODO

        $scope.action = 'editieren';
        console.log($stateParams);
        $scope.book_id = $stateParams.book_id;

        $scope.reset = function () {
            $scope.book = {};
            db.books.findOne({_id: $scope.book_id}, function (err, doc) {
                $scope.book = doc;
            }); //refind
        };

        $scope.reset();

        $scope.submit = function (e) {
            console.log('Speichere');
            if(!Array.isArray($scope.book.author)) {
                $scope.book.author = $scope.book.author.split(', ').map(function (s) { //comma seperated string to array
                    return s.trim()
                });
            }
            if (!$scope.book.thumb || $scope.book.thumb === '') {
                $scope.book.thumb = 'https://books.google.de/googlebooks/images/no_cover_thumb.gif'; //Default pic
            }

            db.books.update({_id: $scope.book_id}, $scope.book);
            LxNotificationService.notify('Medium wurde gespeichert!');
            $scope.reset(); //Todo redirect
            e.preventDefault();
        };
    });
