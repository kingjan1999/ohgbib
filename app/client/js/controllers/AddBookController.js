/**
 * Created by jan on 13.09.15.
 */
angular.module('app.controllers')

    .controller('AddBookController',function ($scope, electron, $timeout, LxNotificationService) {
        //var api_key = "AIzaSyACpwb_vQe8cGu_v3GcQ5nbicrVZ6wKp7A"; //TODO

        $scope.action = 'hinzufügen';

        $scope.reset = function () {
            $scope.book = {
                "title": '',
                "subtitle": '',
                "typ": 'book',
                "author": '',
                "publisher": '',
                "hrsg": '',
                "edition": '',
                "ort": '',
                "year": '',
                "price": '',
                "interest": [],
                "pages": '',
                "description": '',
                "thumb": '',
                "isbn": ''
            };
        };

        $scope.reset(); //Clear form



        $scope.filter = function (a,b) {
            console.log(a, b);
        };

        $scope.submit = function (e) {
            console.log('Speichere');
            $scope.book.author = $scope.book.author.split(', ').map(function (s) { //comma seperated string to array
                return s.trim()
            });
            if (!$scope.book.thumb || $scope.book.thumb === '') {
                $scope.book.thumb = 'https://books.google.de/googlebooks/images/no_cover_thumb.gif'; //Default pic
            }
            db.books.insert($scope.book, function (err, newDoc) {
                LxNotificationService.notify('Medium wurde gespeichert!');
                $scope.reset(); //Todo redirect
            });
            e.preventDefault();
        };

        $scope.searchISBN = function (isbn) {
            if (typeof(isbn) === 'undefined' || isbn === '')
                return;
            console.log('Suche!');
            isbn = isbn.replace(/-*/ig, '');
            var isbn_p = isbnp.parse(isbn);
            if (!isbn_p || !isbn_p.isValid()) {
                console.log('invalid');
                return;
            }

            isbner.resolve(isbn, function (err, book) {
                if (err) {
                    console.log(err);
                } else {
                    console.log(book);
                    $scope.book.isbn = isbn_p.asIsbn13(true);
                    if (book.publisher && $scope.book.publisher === '') {
                        $scope.book.publisher = book.publisher;
                    }

                    if ($scope.book.description === '') {
                        $scope.book.description = book.description;
                    }
                    if ($scope.book.author === '') {
                        $scope.book.author = book.authors.join(', ');
                    }
                    if ($scope.book.year === '' && book.publishedDate) {
                        if (moment(book.publishedDate).isValid() && moment(book.publishedDate).year() !== 1970)
                            $scope.book.year = moment(book.publishedDate);
                        else
                            $scope.book.year = book.publishedDate;
                    }
                    if ($scope.book.title === '') {
                        $scope.book.title = book.title;
                    }
                    if ($scope.book.pages === '') {
                        $scope.book.pages = (book.printedPageCount ? book.printedPageCount : book.pageCount);
                    }
                    if ($scope.book.interest === '' && book.categories) {
                        $scope.book.interest = book.categories;
                    }

                    if (book.imageLinks && book.imageLinks.thumbnail) {
                        $scope.book.thumb = book.imageLinks.thumbnail;
                    }

                    if ($scope.book.subtitle === '') {
                        $scope.book.subtitle = book.subtitle;
                    }

                    if ($scope.book.ort === '' && book.city) {
                        $scope.book.ort = book.city;
                    }

                    if ($scope.book.edition === '' && book.edition) {
                        $scope.book.edition = book.edition;
                    }

                    $timeout(function () {
                        $scope.$apply()
                    });
                }
            });

        };
    });
