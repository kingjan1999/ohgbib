/**
 * Created by jan on 13.09.15.
 */
angular.module('app.controllers')
    .controller('AddUserController',function ($scope, electron, $timeout, LxNotificationService) {
        $scope.reset = function () {
            $scope.user = {
                vorname: '',
                nachname: '',
                typ: 'student',
                course: '',
                barcode: '',
                other: ''
            };
        };

        $scope.action = 'hinzufügen';

        $scope.reset();


        $scope.submit = function (e) {
            console.log('Speichere');
            db.users.insert($scope.user, function (err, newDoc) {
                console.log("Error: ", err);
                console.log("Doc: ", newDoc);
                LxNotificationService.notify('Benutzer wurde gespeichert!');
                $scope.reset(); //Todo redirect
            });
            e.preventDefault();
        }
    });
