/**
 * Created by jan on 22.11.15.
 */
angular.module('app.controllers')

    .controller('LendController', function ($scope, $q, electron, $timeout, LxNotificationService, LendService) {
        $scope.lend = {
            book: '',
            user: ''
        };

        $scope.loading = false;
        $scope.bloading = true;

        $scope.getMatches = function (searchText) {
            if (searchText.length < 2) {
                $scope.users = [];
                return;
            }
            var regex = new RegExp(searchText, "ig");
            $scope.loading = true;
            db.users.find({
                $or: [
                    {vorname: {$regex: regex}},
                    {nachname: {$regex: regex}},
                    {barcode: searchText}
                ]
            }, function (err, users) {
                if (err) {
                    console.log(err);
                    return;
                }
                $scope.users = users.map(function (user) {
                    user.value = user._id;
                    user.display = user.vorname + " " + user.nachname;
                    return user;
                });
                $scope.loading = false;
            });
        };
        $scope.users = $scope.getMatches('');

        $scope.getBarCodeMatches = function (searchText) {
            if (searchText.length < 2) {
                $scope.books = [];
                return;
            }
            var regex = new RegExp(searchText, "ig");
            $scope.bloading = true;
            db.books.find({
                $or: [
                    {title: {$regex: regex}},
                    {author: {$regex: regex}},
                    {barcode: searchText},
                    {isbn: searchText}
                ]
            }, function (err, books) {
                if (err) {
                    console.log(err);
                    return;
                }
                $scope.books = books.map(function (book) {
                    book.value = book._id;
                    book.display = book.title + " (" + book.author + ")";
                    return book;
                });
                $scope.bloading = false;
            });
        };

        $scope.books = $scope.getBarCodeMatches('');

        $scope.submit = function (e) {
            console.log('Alles in Lot auf dem Boot');
            LendService.isLend($scope.lend.book._id).then(function (val) {
                if (val) {
                    console.log('OOps ', val);
                    LxNotificationService.notify("Dieses Buch ist bereits verliehen");
                    throw "Lend";
                } else {
                    console.log('Frei', val);
                    return LendService.calculateUntil();
                }
            }).then(function (until) {
                console.log('Until ' , until);
                return LendService.addLend($scope.lend.book._id, $scope.lend.user._id, until.valueOf());
            }).then(function (doc) {
                LxNotificationService.notify("Entleihung erfolgreich");
            }).catch(function (e) {
                console.log('Error ', e);
                LxNotificationService.notify("Ein Fehler ist aufgreten!");
            });
            e.preventDefault();
        }
    });
