/**
 * Created by jan on 13.09.15.
 */
angular.module('app.controllers')
    .controller('ListUsersController',function ($scope, electron, LxNotificationService, LxDialogService, users, $state) {
        $scope.users = users;

        $scope.selected = {};
        $scope.translations = {
            "vorname": "Vorname",
            'nachname': 'Nachname',
            'typ': 'Typ',
            'course': 'Klasse',
            'barcode': 'Barcode',
            'other': 'Sonstiges',
            '_id': 'ID in der Datenbank'
        };

        $scope.showUser = function (user, ev) {
            console.log(user);
            $scope.keys = Object.keys(user);
            $scope.keys = $scope.keys.remove('$$hashKey');
            $scope.keys = $scope.keys.remove('lendings');
            $scope.selected = user;
            LxDialogService.open('showUser');
        };

        $scope.editUser = function (id) {
            LxDialogService.close('showUser');
            $state.go('editUser', {user_id: id});
        }
    });
