/**
 * Created by jan on 22.11.15.
 */
angular.module('app.controllers')

    .controller('BackController', function ($scope, $q, electron, $timeout, LxNotificationService, LendService) {
        $scope.back = {
            book: ''
        };

        $scope.bloading = true;

        $scope.getBarCodeMatches = function (searchText) {
            if (searchText.length < 2) {
                $scope.books = [];
                return;
            }
            var regex = new RegExp(searchText, "ig");
            $scope.bloading = true;
            db.books.find({
                $or: [
                    {title: {$regex: regex}},
                    {author: {$regex: regex}},
                    {barcode: searchText},
                    {isbn: searchText}
                ]
            }, function (err, books) {
                if (err) {
                    console.log(err);
                    return;
                }
                $scope.books = books.map(function (book) {
                    book.value = book._id;
                    book.display = book.title + " (" + book.author + ")";
                    return book;
                });
                $scope.bloading = false;
            });
        };

        $scope.books = $scope.getBarCodeMatches('');

        $scope.updateUser = function () {
            var book = $scope.back.book;
            LendService.whoLend(book._id).then(function (val) {
               $scope.user = val;
            });
        }

        $scope.submit = function (e) {
            LendService.isLend($scope.back.book._id).then(function (val) {
                if (!val) {
                    LxNotificationService.notify("Dieses Buch ist nicht verliehen");
                } else {
                    return LendService.giveBack($scope.back.book._id);
                }
            }).then(function (doc) {
                if (doc) {
                    LxNotificationService.notify("Rückgabe erfolgreich");
                }
            }).catch(function () {
                LxNotificationService.notify("Ein Fehler ist aufgreten!");
            });
            e.preventDefault();
        }
    });
