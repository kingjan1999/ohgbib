/**
 * Created by jan on 25.11.15.
 */
//const fs = require('fs');

angular.module('app.controllers')

    .controller('SettingsController', function ($scope, electron, $timeout, LxNotificationService, $q, LendService, SettingsService) {


        $scope.settings = {
            'workDays': true,  //Nur Werktage zählen?
            'lendDays': 10, //Anzahl der Ausliehtage
            'max_per_user': 3, // Maximale Entleihungen pro Benutzer
            'ferien': [], //Ferien-Objekte
            'jStart': 5, //Erster Jahrgang
            'jEnd': 12, //Letzter Jahrgang
            'waitBeforeDelete': 1 //Jahre, die gewartet werden soll, bis der letzte Jahrgang endgültig entfernt wird
        };

        $scope.ferien = [];

        LendService.getFerien(moment().year()).then(function (fer) {
            fer.forEach(function (va) {
                $scope.ferien.push(va);
            });
            return LendService.getFerien(moment().year() + 1);
        }).then(function (ferien) {
            ferien.forEach(function (va) {
                $scope.ferien.push(va);
            });

            $scope.ferien.forEach(function (ferie, key) {
                $scope.settings.ferien[key] = {};
                $scope.settings.ferien[key].beginn = ferie.mbegin.toISOString();
                $scope.settings.ferien[key].ende = ferie.mende.toISOString();
            })
        });

        $scope.formFields = [
            {
                key: 'lendDays',
                type: 'lx-input',
                templateOptions: {
                    type: 'number',
                    label: 'Ausleihtage',
                    required: true
                }
            },
            {
                key: 'workDays',
                type: 'lx-switch',
                templateOptions: {
                    label: 'Werktage',
                    description: 'Nur Werktage zählen'
                }
            },
            {
                key: 'max_per_user',
                type: 'lx-input',
                templateOptions: {
                    type: 'number',
                    label: 'Maximale Entleihungen pro Benutzer',
                    required: true
                }
            }, {
                key: 'jStart',
                type: 'lx-input',
                templateOptions: {
                    type: 'number',
                    label: 'Erster Jahrgang',
                    required: true
                }
            }, {
                key: 'jEnd',
                type: 'lx-input',
                templateOptions: {
                    type: 'number',
                    label: 'Letzter Jahrgang',
                    required: true
                }
            }, {
                key: 'waitBeforeDelete',
                type: 'lx-input',
                templateOptions: {
                    type: 'number',
                    label: 'Jahre, die gewartet werden sollen, bis der jeweils letzte Jahrgang entfernt wird',
                    description: 'Jahre, die gewartet werden sollen, bis der jeweils letzte Jahrgang entfernt wird. -1 = nie, 0 = sofort',
                    required: true
                }
            }
        ];

        SettingsService.read().then(function (object) {
            _.merge($scope.settings, object);
        });

        $scope.submit = function (e) {
            e.preventDefault();
            SettingsService.write($scope.settings).then(function () {
                LxNotificationService.notify('Speichern erfolgreich');
            }).catch(function (err) {
                LxNotificationService.notify('Ein Fehler ist aufgetreten');
                console.log(err);
            })
        }

    });
