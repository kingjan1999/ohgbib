/**
 * Created by jan on 13.09.15.
 */


angular.module('app.controllers')
    .controller('ListBooksController', function ListBooksCtrl($scope, electron, LxNotificationService, LxDialogService, LendService, books, $state) {

        $scope.books = books;

        $scope.selectedBooks = [];

        $scope.isSelected = function (book) {
            if (!book) {
                return;
            }
            return $scope.selectedBooks.indexOf(book) > -1;
        };

        $scope.select = function (book) {
            if (!book) {
                return;
            }
            if (!$scope.isSelected(book)) {
                $scope.selectedBooks.push(book);
            } else {
                $scope.selectedBooks.splice($scope.selectedBooks.indexOf(book), 1);
            }
        };

        $scope.translations = {
            "title": "Titel",
            "subtitle": "Untertitel",
            "typ": "Typ",
            "author": "Autor",
            "publisher": "Verlag",
            "hrsg": "Herausgeber",
            "edition": "Auflage",
            "ort": "Ort",
            "year": "Erscheinungsjahr",
            "price": "Preis",
            "interest": "Kategorien",
            "pages": "Seiten",
            "isbn": "ISBN",
            "signature": "Signatur",
            "barcode": "Barcode",
            "_id": "ID in der Datenbank",
            "regisseur": "Regisseur",
            "actors": "Hauptdarsteller",
            "actors2": "Nebendarsteller",
            "length": "Spieldauer (min.)",
            "description": "Beschreibung",
            "isLend": "Verliehen"
        };

        /*db.books.find({}, function (err, newDocs) {
         $scope.books = newDocs;
         }); */

        $scope.showBook = function (book, ev) {
            console.log(book);
            $scope.keys = Object.keys(book);
            $scope.keys = $scope.keys.remove('$$hashKey');
            $scope.keys = $scope.keys.remove('thumb');
            $scope.keys = $scope.keys.remove('lendings');
            $scope.selected = book;
            LxDialogService.open('showBook');
        };

        $scope.export = function () {
            console.log('Sende signal');
            electron.dialog.showSaveDialog({
                title: 'Als PDF exportieren',
                defaultPath: 'exported.pdf',
                filters: [
                    {name: 'PDF-Dateien', extensions: ['pdf']}
                ]
            }, function (filepath) {
                if(filepath) {
                    setTimeout(function () {
                        electron.send(['print-pdf', filepath]);
                        setTimeout(function () {
                            $scope.onlySelected = false; //Reset selected after timeout
                        }, 1000);
                    }, 100);
                }
            })
        };

        $scope.print = function () {
            setTimeout(function () {
                window.print();
                $scope.onlySelected = false;
            }, 100);
        };

        $scope.giveBack = function (book) {
            LendService.giveBack(book).then(function () {
                LxNotificationService.notify('Buch wurde zurückgegeben!');
            });
        };

        $scope.editMedium = function () {
            LxDialogService.close('showBook');
            $state.go('editMedium', {book_id: $scope.selected._id});
        };

        $scope.onlySelected = false;

        $scope.exportSelected = function () {
            $scope.onlySelected = true;
            $scope.export();
        }

        $scope.printSelected = function () {
            $scope.onlySelected = true;
            $scope.print();
        }
    }
    );


Array.prototype.remove = function () {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};
