/**
 * Amy is Awesome!
 */
var Datastore = require('nedb')
    , path = require('path')
    , db = {};
var jetpack = require('fs-jetpack');
var fs = require('fs');

console.log('Geladen');
//moment.locale('de');

angular.module('app',
    [
        "ngElectron",
        "lumx",
        "ui.router",
        "app.controllers",
        "app.directives",
        "app.services",
        "app.routes",
        "app.filters",
        "formly",
        "formlyLumx",
        "angularMoment"
    ])
    .config(function (electronProvider) {
        var app = electronProvider.getObject().app;
        var datenPfad = app.getDataPath();
        console.log(datenPfad);

        db.books = new Datastore({filename: path.join(datenPfad, 'books.db')});
        db.users = new Datastore({filename: path.join(datenPfad, 'users.db')});
        db.lends = new Datastore({filename: path.join(datenPfad, 'lends.db')});
        db.settings = new Datastore({filename: path.join(datenPfad, 'settings.db')});

        db.books.loadDatabase();
        db.users.loadDatabase();
        db.lends.loadDatabase();
    })
    .run(function (electron, amMoment) {
        amMoment.changeLocale('de');
    });
