angular.module('app.routes', [])
    .config(['$stateProvider', '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {
            $stateProvider
                .state('index', {
                    url: "",
                    templateUrl: "client/tpl/main.html"
                })
                .state('addMedium', {
                    url: "/add",
                    templateUrl: "client/tpl/add.html",
                    controller: "AddBookController"
                })
                .state('editMedium', {
                    url: "/edit/:book_id",
                    templateUrl: "client/tpl/add.html",
                    controller: "EditBookController"
                })
                .state('addUser', {
                    url: "/addUser",
                    templateUrl: "client/tpl/addUser.html",
                    controller: "AddUserController"
                })
                .state('editUser', {
                    url: "/editUser/:user_id",
                    templateUrl: "client/tpl/addUser.html",
                    controller: "EditUserController"
                })
                .state('listBooks', {
                    url: "/addBooks",
                    templateUrl: "client/tpl/listBooks.html",
                    controller: "ListBooksController",
                    resolve: {
                        books: function ($q, LendService, UserService) {
                            var deferred = $q.defer();
                            db.books.find({}, function (err, books) { //Bücher
                                if (err) {
                                    deferred.reject(err);
                                } else {
                                    var proms = [];
                                    books.forEach(function (book) {  //für jedes Buch
                                        proms.push(LendService.getHistory(book._id).then(function (hist) { //Leihistorie
                                            book.lendings = hist;
                                            var promises = [];
                                            var isLend = false;
                                            hist.forEach(function (his) { //Für jeden Eintrag
                                                if(his.active) {
                                                    isLend = true;
                                                }
                                                promises.push(UserService.byId(his.user).then(function (u) { //Gette den Benutzer
                                                    his.user = u;
                                                    return his;
                                                }));
                                            });
                                            return $q.all(promises).then(function (res) {
                                                book.lendings = res;
                                                book.isLend = isLend;
                                                return book;
                                            });
                                        }));
                                    });
                                    $q.all(proms).then(function (books) {
                                        deferred.resolve(books);
                                    })
                                }
                            });
                            return deferred.promise;
                        }
                    }
                })
                .state('listUsers', {
                    url: "/listUsers",
                    templateUrl: "client/tpl/listUsers.html",
                    controller: "ListUsersController",
                    resolve: {
                        users: function ($q, LendService, UserService) {
                            var deferred = $q.defer();
                            db.users.find({}, function (err, users) {
                                if (err) {
                                    deferred.reject(err);
                                } else {
                                    var proms = [];
                                    users.forEach(function (user) {
                                        proms.push(UserService.getBookLendings(user._id).then(function (lendings) {
                                            user.lendings = lendings;
                                            return user;
                                        }));
                                    });
                                    return $q.all(proms)
                                        .then(function (users) {
                                            deferred.resolve(users);
                                        });
                                }
                            });
                            return deferred.promise;
                        }
                    }
                })
                .state('lend', {
                    url: "/lend",
                    templateUrl: "client/tpl/lend.html",
                    controller: "LendController"
                })
                .state('back', {
                    url: "/back",
                    templateUrl: "client/tpl/back.html",
                    controller: "BackController"
                })
                .state('settings', {
                    url: "/settings",
                    templateUrl: "client/tpl/settings.html",
                    controller: "SettingsController"
                });
        }]);
