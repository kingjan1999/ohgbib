/**
 * Created by jan on 25.11.15.
 */
var gulp = require('gulp');
var run = require('gulp-run');
var livereload = require('gulp-livereload');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var bower = require('gulp-bower');
var runSequence = require('run-sequence');
var del = require('del');
var gulpSequence = require('gulp-sequence');
var ngAnnotate = require('gulp-ng-annotate');
var zip = require('gulp-zip');
var electron = require('gulp-electron');
var bowerFiles = require('main-bower-files'),
    angularFilesort = require('gulp-angular-filesort'),
    inject = require('gulp-inject');
var packageJson = require('./app/package.json');


gulp.task('copy-electron', function () {
    gulp.src('index.html', {cwd: 'app/'})
        .pipe(inject(gulp.src(bowerFiles({
            paths: {
                bowerDirectory: 'app/client/lib',
                bowerrc: 'app/client/.bowerrc',
                bowerJson: 'app/client/bower.json'
            }
        }), {read: false, cwd: 'app/'}), {name: 'bower', relative: true}))
        .pipe(inject(
            gulp.src('client/js/**/*.js', {cwd: 'app/'}) // gulp-angular-filesort depends on file contents, so don't use {read: false} here
                .pipe(angularFilesort()),
            {relative: true}))
        .pipe(gulp.dest('./dist/'))
        .pipe(livereload());

    gulp.src(['index.css', 'index.js', 'package.json'], {cwd: 'app/'})
        .pipe(gulp.dest('dist/'))
        .pipe(livereload());

    gulp.src(['./node_modules/**/*'], {cwd: 'app'})
        .pipe(gulp.dest('dist/node_modules/'))
});

gulp.task('copy-html', function () {
    return gulp.src('client/**/*.html', {cwd: 'app/'})
        // Perform minification tasks, etc here
        //.pipe(uglify())
        .pipe(gulp.dest('dist/client/'))
        .pipe(livereload());
});

gulp.task('copy-js', function () {
    return gulp.src('client/js/**/*.js', {cwd: 'app/'})
        // Perform minification tasks, etc here
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(gulp.dest('dist/client/js/'))
        .pipe(livereload());
});

gulp.task('sass', function () {
    return gulp.src('client/**/*.scss', {cwd: 'app/'})
        .pipe(sass({
            includePaths: ['app/client/lib/bourbon/app/assets/stylesheets/', 'app/client/lib/mdi/scss/']
        }).on('error', sass.logError))
        .pipe(gulp.dest('dist/client/'))
        .pipe(livereload());
});

gulp.task('sass:watch', function () {
    return gulp.watch('client/**/*.scss', ['sass'], {cwd: 'app/'});
});

gulp.task('bower', function () {
    return bower({directory: './lib', cwd: './app/client/'})
        .pipe(gulp.dest('dist/client/lib'))
});

gulp.task('run', function () {
    run('electron dist/').exec();
});

gulp.task('watch', function () {
    livereload.listen();
    gulp.watch('app/client/**/*.html', ['copy-html']);
    gulp.watch('app/client/**/*.js', ['copy-js']);
    gulp.watch('app/index.*', ['copy-electron']);
});

gulp.task('clean', function () {
    return del(['dist/*', 'release/*']);
});

gulp.task('zip', function () {
    return gulp.src('dist/*')
        .pipe(zip('output.zip'))
        .pipe(gulp.dest('./'));
});

gulp.task('electron', function () {

    gulp.src("")
        .pipe(electron({
            src: './dist',
            packageJson: packageJson,
            release: './release',
            cache: './cache',
            version: 'v0.36.0',
            packaging: true,
            platforms: ['win32-ia32', 'win32-x64', 'linux-x64', 'linux-ia32'],
            platformResources: {
                darwin: {
                    CFBundleDisplayName: packageJson.name,
                    CFBundleIdentifier: packageJson.name,
                    CFBundleName: packageJson.name,
                    CFBundleVersion: packageJson.version,
                    icon: 'assets/mac.icns'
                },
                win: {
                    "version-string": packageJson.version,
                    "file-version": packageJson.version,
                    "product-version": packageJson.version,
                    "icon": 'assets/windows.ico'
                }
            }
        }))
        .pipe(gulp.dest(""));
});


gulp.task('build', gulpSequence('clean', 'copy-electron', ['copy-html', 'copy-js', 'sass', 'bower']));

gulp.task('default', gulpSequence('build', ['watch', 'sass:watch', 'run']));

