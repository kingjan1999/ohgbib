module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        'create-windows-installer': {
            x64: {
                appDirectory: './release/v0.36.0/win32-x64',
                outputDirectory: 'release/windows/64',
                authors: 'Jan Beckmann',
                exe: 'OHGBIB.exe'
            },
            ia32: {
                appDirectory: './release/v0.36.0/win32-ia32',
                outputDirectory: 'release/windows/32',
                authors: 'Jan Beckmann',
                exe: 'OHGBIB.exe'
            }
        },
        'electron-debian-installer': {
            options: {
                rename: function (dest, src) {
                    return dest + '<%= name %>_<%= version %>-<%= revision %>_<%= arch %>.deb';
                },
                productName: 'OHGBIB'
            },
            linux32: {
                options: {
                    arch: 'i386'
                },
                src: 'release/v0.36.0/linux-ia32/',
                dest: 'release/linux/'
            },
            linux64: {
                options: {
                    arch: 'amd64'
                },
                src: 'release/v0.36.0/linux-x64/',
                dest: 'release/linux/'
            }

        }
    });

    grunt.loadNpmTasks('grunt-electron-installer');
    grunt.loadNpmTasks('grunt-electron-debian-installer');
    // Default task(s).
    grunt.registerTask('default', ['create-windows-installer']);

};
